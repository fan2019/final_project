package steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.junit.Assert;

public class LoginSteps extends Steps{

    private boolean result;
    private WebLogin webLogin;

    @Given("open the loginStep page")
    public void givenOpenTheLoginPage() {
        webLogin = new WebLogin("2359448y@student.gla.ac.uk","yf19921215");
    }

    @When("I enter the username as <username>")
    public void whenIEnterTheUsernameAsusername(@Named("username") String username) {
        webLogin.enterUserName(username);
    }

    @When("I enter the password as <password>")
    public void whenIEnterThePasswordAspassword(@Named("password") String password) {
        webLogin.enterPassWord(password);
    }

    @When("I click loginStep button")
    public void whenIClickLoginButton() {
        webLogin.clickLoginButton();
    }

    @Then("I should loginStep successful as <result>")
    public void thenIShouldLoginSuccessfulAsresult(@Named("result") boolean result) {
        this.result = webLogin.getRusult();
        Assert.assertSame(this.result,result);
    }
}
