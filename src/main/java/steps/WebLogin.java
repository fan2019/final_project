package steps;

//this is a test class to simulate web login page
public class WebLogin {

    // simulate a sample database of user
    private String userName;
    private String passWord;
    // store the enter
    private String enterName = "";
    private String enterPass = "";
    // result
    private boolean isSuccessfulLogin = false;

    public WebLogin(String name, String pass){
        this.userName = name;
        this.passWord = pass;
    }

    public void clickLoginButton(){
        System.out.println("click the login button");
        if (this.enterName.equals(this.userName) && this.enterPass.equals(this.passWord)){
            isSuccessfulLogin = true;
        }else{
            isSuccessfulLogin = false;
        }
    }

    public void enterUserName(String userName){
        this.enterName = userName;
        System.out.println("Enter UserName: "+userName);
    }

    public void enterPassWord(String passWord){
        this.enterPass = passWord;
        System.out.println("Enter passWord: "+passWord);
    }

    public boolean getRusult(){

        System.out.println("It should can login is "+this.isSuccessfulLogin);
        return this.isSuccessfulLogin;
    }
}
