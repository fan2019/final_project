
import mutators.mutatorForStory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import workFlow.getCSVResult;
import workFlow.makeRefactorStory;
import workFlow.runStories;

public class RunTheEntireProject{

    // parsing the original story and make the mutation story
    @Before
    public void Refactor(){
        System.out.println("step:1");
        makeRefactorStory.makeRefactorStory();

    }
    // run all stories
    @Test
    public void RunStory(){
        System.out.println("step:2");
        runStories runStories = new runStories();
        runStories.runSerenity().inASingleSession();
    }

    // get the csv result and parsing the result, then generate a allow story
    @After
    public void getAllowStory(){
        System.out.println("step:3");
        // get the result
        getCSVResult.getCSVResult();
        // Generate a allow story
        getCSVResult.getAllowScenarios();
    }

    // do the mutator
    @AfterClass
    public static void mutator(){
        System.out.println("step:4");
        mutatorForStory.mutator();
    }

}
