package fileOperation;

import java.io.File;

public class CurrentPath {


    public static String getTheHardPath(){

        File file = new File(".");

        String path = file.getAbsoluteFile().getPath();

        return path.substring(0,path.length()-1);
    }

}
