package fileOperation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class CreateStoryFile {

    public static void WirteFile(String content, File file){

        try {
            FileWriter writer = new FileWriter(file, false);
            BufferedWriter out = new BufferedWriter(writer);
            out.write(content);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println("write err!");
            e.printStackTrace();
        } finally {
            if (file == null) {
                System.err.println("pleace create a file first");
            }
        }
    }
}
