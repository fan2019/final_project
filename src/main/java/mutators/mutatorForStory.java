package mutators;

import fileOperation.CreateStoryFile;
import fileOperation.CurrentPath;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class mutatorForStory {

    private static Embedder embedder = new Embedder();
    private static String refactorStoryPath = "stories/originalStory.story";
    private static List<Scenario> scenarios;
    private static ArrayList<ArrayList<String>> Story = new ArrayList<>();


    // embed the refactor Story
    private static Story getRefactorStroy(){

        return embedder.storyManager().storyOfPath(refactorStoryPath);

    }


    // Parse this story to get all the scenarios
    private static void parsingStory(){

        scenarios = getRefactorStroy().getScenarios();

    }

    // Parse all scenes into an ArrayList
    private static void parsingAllScenariosToList(){

        parsingStory();

        for (Scenario scenario:scenarios) {

            ArrayList<String> scenarioAsString = new ArrayList<>();

            scenarioAsString.add("Scenario: "+scenario.getTitle());

            for (String step:scenario.getSteps()) {

                scenarioAsString.add(step);

            }

            scenarioAsString.add("Examples: \n"+scenario.getExamplesTable().asString());

            Story.add(scenarioAsString);
        }

    }

    // operating
    private static void addingOrRemoveLines(String operating,int scenarios, int step){

           if (operating.equals("Removing")){

               Story.get(scenarios).remove(step);
               Story.get(scenarios).add(step,"!--"+Story.get(scenarios).get(step));

           }else if(operating.equals("Adding")){

               System.out.println("Please, enter what your want to add:");

               Scanner scanner = new Scanner(System.in);

               String add = scanner.nextLine();

               Story.get(scenarios).add(step,add);
           }
    }

    public static void mutator(){

        parsingAllScenariosToList();

        System.out.println("Please, enter your operating (Removing or Adding or NOT):");

        Scanner scanner = new Scanner(System.in);

        String operating = scanner.nextLine();

        if (operating.equals("NOT")){System.exit(0);}

        int NumScenario;

        do {
            System.out.println(String.format("Please, enter which scenario your want to change (int only, 0~%d):",getMaxOfNumScenarios()));
            NumScenario = scanner.nextInt();
        }while (NumScenario < 0 || NumScenario > getMaxOfNumScenarios());

        int NumStep;

        do {
            System.out.println(String.format("Please, enter which Step your want to change (int only, 1~%d):",getMaxOfNumStep()));
            NumStep = scanner.nextInt();
        }while (NumStep <= 0 || NumStep > getMaxOfNumStep());

        addingOrRemoveLines(operating,NumScenario,NumStep);

        String wholeStory = "";

        for (ArrayList<String> scenario: Story) {

            for (String s: scenario) {

                wholeStory += s+"\n";
            }
        }

        File file = new File(CurrentPath.getTheHardPath()+"src/main/java/mutators/mutators.story");
        CreateStoryFile.WirteFile(wholeStory,file);

    }

    private static void runMutatorStory(){

        // same as


    }

    private static int getMaxOfNumScenarios(){

        return Story.size()-1;
    }

    private static int getMaxOfNumStep(){

        return Story.get(0).size()-2;
    }

    public static void main(String[] args) {

        mutator();

    }

}
