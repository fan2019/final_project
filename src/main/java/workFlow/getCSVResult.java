package workFlow;

import fileOperation.CreateStoryFile;
import fileOperation.CurrentPath;
import net.thucydides.core.reports.OutcomeFormat;
import net.thucydides.core.reports.TestOutcomeLoader;
import net.thucydides.core.reports.TestOutcomes;
import parser.CSVReporter;
import parser.ParsingTheInitialStory;
import processResult.ParserResult;
import processResult.ParserString2Story;

import java.io.File;

public class getCSVResult {

    private static String hardPath = CurrentPath.getTheHardPath();

    public static void main(String[] args) {

        getCSVResult();

        getAllowScenarios();
    }

    public static void getCSVResult(){

        CSVReporter csvReporter = new CSVReporter(new File(hardPath+"target/test-classes/"));
        OutcomeFormat format = OutcomeFormat.JSON;
        TestOutcomeLoader.TestOutcomeLoaderBuilder builder = TestOutcomeLoader.loadTestOutcomes().inFormat(format);
        try {
            TestOutcomes outcomes = builder.from(new File(hardPath+"target/site/serenity/"));
            csvReporter.generateReportFor(outcomes,"try.csv");
        } catch (Exception e){
            System.out.println("DIDNT WORK");
            e.printStackTrace();
        }
    }

    public static void getAllowScenarios(){

        ParserResult parserResult = new ParserResult(hardPath+"target/test-classes/try.csv");
        String refactorStory =  ParsingTheInitialStory.parsingTheInitialStory("stories/originalStory.story");
        ParserString2Story parserString2Story = new ParserString2Story(refactorStory);

        parserString2Story.identify(parserResult.getResultsList());

        System.out.println(parserString2Story.getAllowStory());

        // write the whole all scenarios to allow Story
        File file = new File(hardPath+"src/main/resources/allow/allow.story");

        CreateStoryFile.WirteFile(parserString2Story.getAllowStory(),file);

    }

}
