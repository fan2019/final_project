package workFlow;

import fileOperation.CreateStoryFile;
import fileOperation.CurrentPath;
import parser.ParsingTheInitialStory;

import java.io.File;

public class makeRefactorStory {

    private static String hardPath = CurrentPath.getTheHardPath();

    public static void main(String[] args) {
        makeRefactorStory();
    }

    public static void makeRefactorStory(){

        // get the refactor story and return a String
        String refactorStory =  ParsingTheInitialStory.parsingTheInitialStory("stories/originalStory.story");
        // write the story to refactorStory
        File reStory = new File(hardPath+"src/main/resources/stories/refactorStory.story");
        CreateStoryFile.WirteFile(refactorStory,reStory);

    }



}
