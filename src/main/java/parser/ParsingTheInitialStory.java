package parser;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;

import java.util.*;

public class ParsingTheInitialStory {


    private static Embedder embedder = new Embedder();
    private static ArrayList<String> newCondition = new ArrayList<>();
    private static ArrayList<String> newScenarios = new ArrayList<>();
    private static String GivenString = "";
    private static String ThenString = "";
    private static String RefactorStory;
    private static final String NL = "\n";
    private static int indexOfMutation = 0;

    public static String parsingTheInitialStory(String originalStoryPath){
        Story story = embedder.storyManager().storyOfPath(originalStoryPath);

        for (Scenario scenario:story.getScenarios()) {

            parsingToString(scenario);
        }

        return RefactorStory;

    }

    private static void parsingToString(Scenario scenario){
        // Reset newCondition
        newCondition = new ArrayList<>();
        String title = "Scenario: Mutation Scenarios ";
        List<String> steps = scenario.getSteps();
        ArrayList<String> listOfCondition = new ArrayList<>();
        for (String step : steps) {

            if (step.startsWith("When")) {
                listOfCondition.add(step);
            }

            if (step.startsWith("Given")){
                GivenString = step;
            }

            if (step.startsWith("Then")){
                ThenString = step;
            }
        }

        int size = listOfCondition.size();

        String[] list;

        list = listOfCondition.toArray(new String[size]);

        String table = "Examples:" + NL + scenario.getExamplesTable().asString() + NL;

        permutation(list,0);

        //refactor the list make each element to be a scenario

        for (String condition:newCondition) {
            indexOfMutation++;

            String con = StringUtils.strip(condition,"[]");

            newScenarios.add(title+indexOfMutation+NL+GivenString+NL+con.replaceAll(", "," \n")+NL+ThenString+NL+table);

        }

        RefactorStory = StringUtils.strip(newScenarios.toString(), "[]").replaceAll(", ", "");

    }

    private static void permutation(String[] array, int start) {

        if (start == array.length) {
            newCondition.add(Arrays.toString(array));
        } else
            for (int i = start; i < array.length; ++i) {
                swap(array, start, i);
                permutation(array, start + 1);
                swap(array, start, i);
            }
    }

    private static void swap(String[] array, int s, int i) {
        String t = array[s];
        array[s] = array[i];
        array[i] = t;
    }

    public static String ToString(){

        return RefactorStory;
    }



}
