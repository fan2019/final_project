package processResult;

import java.util.ArrayList;

public class FanScenariosResult {

    private String Story = "";
    private String Title ="";
    private String Result = "";
    private String Date = "";
    private String Stability = "";
    private String Duration = "";
    private static ArrayList<String> attributesList;

    public void setDate(String date) {
        Date = date;
    }

    public void setStability(String stability) {
        Stability = stability;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public void setStory(String story) {
        this.Story = story;
    }

    public void setResult(String result) {
        this.Result = result;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public  String getStory() {
        return this.Story;
    }

    public  String getTitle() {
        return this.Title.substring(1,this.Title.length()-1);
    }

    public  String getResult(){

        return this.Result.substring(1,this.Result.length()-1);
    }

    public String getDate() {
        return Date;
    }

    public String getDuration() {
        return Duration;
    }

    public String getStability() {
        return Stability;
    }

    public boolean getIsAllow(){

        if (this.Result.equals("SUCCESS")){
            return true;
        }else {
            return false;
        }
    }

    // Get a list of properties after reading the file, store the list of properties in a dynamic array
    public static void setAttributesList(ArrayList<String> attributesList) {
        FanScenariosResult.attributesList = attributesList;
    }

    public static ArrayList<String> getAttributesList() {
        return attributesList;
    }
}
