package processResult;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParserResult {

    private static ArrayList<FanScenariosResult> resultsList;
    private static ArrayList<String> attributelist;

    public ParserResult(String path){

        ArrayList<ArrayList<String>> alldata = CSV2Array(path);
        resultsList = new ArrayList<FanScenariosResult>(alldata.size());
        //Traversing the array for create processResult.FanScenariosResult object
        for (int i = 0; i<alldata.size();i++){
            FanScenariosResult result = new FanScenariosResult();
            if (i == 0){
                result.setAttributesList(alldata.get(i));
                attributelist = alldata.get(i);
            }else {
                ArrayList<String> arrayList = alldata.get(i);
                Field[] fields = FanScenariosResult.class.getDeclaredFields();
                for (int j=0;j<fields.length;j++){
//                    System.out.println(fields[j].getGenericType().toString());
                    if ((fields[j].getGenericType().toString()).equals("class java.lang.String")){
                        String resultString = arrayList.get(j);
                        String attributeName = fields[j].getName();
                        try {
                            String str = resultString;
                            Method setMethod = FanScenariosResult.class.getDeclaredMethod("set"+attributeName,String.class);
                            setMethod.invoke(result,str);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                resultsList.add(i-1,result);
            }
        }
    }

    // read the CSV result file
    private static ArrayList<ArrayList<String>> CSV2Array(String path){

        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path),"UTF-8"));
            ArrayList<ArrayList<String>> allData = new ArrayList<ArrayList<String>>();
            String line;
            String[] oneRow;
            while ((line = in.readLine()) != null){
                oneRow = line.split(",");
                List<String> oneRowList = Arrays.asList(oneRow);
                ArrayList<String> oneRowArrayList = new ArrayList<String>(oneRowList);
                allData.add(oneRowArrayList);
            }
            in.close();
            return allData;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<FanScenariosResult> getResultsList() {
        return resultsList;
    }

}
