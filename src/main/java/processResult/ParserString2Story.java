package processResult;

import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.parsers.StoryParser;

import java.util.ArrayList;
import java.util.List;


public class ParserString2Story {

    private StoryParser parser = new RegexStoryParser(new LocalizedKeywords());
    private List<Scenario> scenarios;
    private static String allowStory = "";
    private static String NL = "\n";
    private static String examples = "Examples:";

    public ParserString2Story(String wholeStory){

        Story story = parser.parseStory(wholeStory,"stories/refactorStory.story");
        scenarios = story.getScenarios();

    }

    public void identify(ArrayList<FanScenariosResult> result){

        for (int i = 0; i < result.size(); i++) {

            FanScenariosResult ScenariosResult = result.get(i);

            if (ScenariosResult.getResult().equals("SUCCESS")){

                for (Scenario scenario:scenarios) {

                    if (scenario.getTitle().equals(ScenariosResult.getTitle())){

                        allowStory += list2String(scenario)+ NL;

                    }
                }
            }
        }
    }

    public String getAllowStory() {
        return allowStory;
    }

    // parser the ArrayList of scenarios to string scenarios
    private String list2String(Scenario scenario){

        String steps = "";

        for (String step : scenario.getSteps()) {

            steps += step+NL;
        }

        String scenarioString = "Scenario:"+scenario.getTitle()+NL+steps+examples+NL+scenario.getExamplesTable().asString();

        return scenarioString;
    }
}
