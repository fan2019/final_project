Scenario:scenario frist enter username and last click the login button
Given open the loginStep page
When I enter the username as <username>
When I enter the password as <password>
When I click loginStep button
Then I should loginStep successful as <result>
Examples:
|username|password|result|
|2359448y@student.gla.ac.uk|yf19921215|true|

Scenario: change the username and password order on table
Given open the loginStep page
When I enter the username as <username>
When I enter the password as <password>
When I click loginStep button
Then I should loginStep successful as <result>
Examples:
|username|password|result|
|yf19921215|2359448y@student.gla.ac.uk|true|